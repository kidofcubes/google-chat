pluginManagement {
    repositories {
        maven("https://maven.fabricmc.net/") // FabricMC
        maven("https://maven.frohnmeyer-wds.de/artifacts") // scripts
        gradlePluginPortal()
    }
}

rootProject.name = "google-chat"
