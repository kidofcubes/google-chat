import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod") version "1.3-SNAPSHOT"
    id("jf.taskgraph") version "1.3-SNAPSHOT"
}

dependencies {
    modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v1:${prop("libjf_version")}")
    modImplementation("io.gitlab.jfronny.libjf:libjf-translate-v1:${prop("libjf_version")}")
    include(modImplementation(fabricApi.module("fabric-message-api-v1", prop("fabric_version")))!!)

    // Dev env
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-config-ui-tiny-v1:${prop("libjf_version")}")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil:${prop("libjf_version")}")
    modLocalRuntime("com.terraformersmc:modmenu:6.1.0-rc.1")
}
