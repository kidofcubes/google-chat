package io.gitlab.jfronny.googlechat.client.mixin;

import com.mojang.authlib.GameProfile;
import io.gitlab.jfronny.googlechat.GoogleChatCache;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.message.MessageHandler;
import net.minecraft.network.message.MessageType;
import net.minecraft.network.message.SignedMessage;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;

@Mixin(MessageHandler.class)
public class MessageHandlerMixin {
    @Shadow @Final private MinecraftClient client;

    @Redirect(method = "onChatMessage(Lnet/minecraft/network/message/SignedMessage;Lcom/mojang/authlib/GameProfile;Lnet/minecraft/network/message/MessageType$Parameters;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/network/message/MessageType$Parameters;applyChatDecoration(Lnet/minecraft/text/Text;)Lnet/minecraft/text/Text;"))
    Text googlechat$applyDecoration(MessageType.Parameters instance, Text content, SignedMessage args$signed, GameProfile args$sender) {
        return instance.applyChatDecoration(googlechat$shouldTranslate(args$sender) ? GoogleChatCache.s2c(content) : content);
    }

    @Redirect(method = "processChatMessageInternal(Lnet/minecraft/network/message/MessageType$Parameters;Lnet/minecraft/network/message/SignedMessage;Lnet/minecraft/text/Text;Lcom/mojang/authlib/GameProfile;ZLjava/time/Instant;)Z", at = @At(value = "INVOKE", target = "Lnet/minecraft/network/message/MessageType$Parameters;applyChatDecoration(Lnet/minecraft/text/Text;)Lnet/minecraft/text/Text;"))
    Text googlechat$applyDecoration2(MessageType.Parameters instance, Text content, MessageType.Parameters args$params, SignedMessage args$message, Text args$decorated, GameProfile args$sender) {
        return instance.applyChatDecoration(googlechat$shouldTranslate(args$sender) ? GoogleChatCache.s2c(content) : content);
    }

    @ModifyVariable(method = "onProfilelessMessage(Lnet/minecraft/text/Text;Lnet/minecraft/network/message/MessageType$Parameters;)V", at = @At(value = "HEAD"), argsOnly = true, ordinal = 0)
    Text googlechat$applyTranslation(Text origin) {
        return GoogleChatCache.s2c(origin);
    }

    @ModifyVariable(method = "onGameMessage(Lnet/minecraft/text/Text;Z)V", at = @At(value = "HEAD"), argsOnly = true, ordinal = 0)
    Text googlechat$applyTranslation2(Text origin) {
        return GoogleChatCache.s2c(origin);
    }

    private boolean googlechat$shouldTranslate(GameProfile sender) {
        return client != null && client.player != null && sender != null && !sender.getId().equals(client.player.getUuid());
    }
}
