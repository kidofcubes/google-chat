package io.gitlab.jfronny.googlechat;

import io.gitlab.jfronny.commons.serialize.gson.api.v1.Ignore;
import io.gitlab.jfronny.libjf.config.api.v1.*;
import net.fabricmc.api.*;
import net.fabricmc.loader.api.*;

@JfConfig(referencedConfigs = "libjf-translate-v1")
public class GoogleChatConfig {
    @Entry public static boolean enabled = true;
    @Entry public static String serverLanguage = "auto";
    @Entry public static String clientLanguage = "en";
    @Entry public static boolean translationTooltip = false;
    @Entry public static boolean desugar = false;
    @Entry public static String receivingRegex = "";
    @Entry public static boolean receivingRegexIsBlacklist = true;
    @Entry public static String sendingRegex = "";
    @Entry public static boolean sendingRegexIsBlacklist = true;
    @Entry(min = 1, max = 1024) public static int cacheSize = 256;
    @Entry public static boolean debugLogs = FabricLoader.getInstance().isDevelopmentEnvironment();

    @Preset
    public static void client() {
        enabled = true;
        if (!serverLanguage.equals("auto")) {
            serverLanguage = "auto";
            clientLanguage = "en";
            String tmp = receivingRegex;
            receivingRegex = sendingRegex;
            sendingRegex = tmp;
        }
    }

    @Preset
    public static void server() {
        enabled = true;
        if (!clientLanguage.equals("auto")) {
            clientLanguage = "auto";
            serverLanguage = "en";
            String tmp = receivingRegex;
            receivingRegex = sendingRegex;
            sendingRegex = tmp;
        }
    }

    @Ignore private static boolean initial = true;
    @Verifier
    public static void verify() {
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.SERVER && !clientLanguage.equals("auto")) {
            System.err.println("""
                    Your client language is not set to "auto" and you are using a server.
                    This setup is not recommended! Please set up GoogleChat according to its documentation!""");
        }
        if (!initial) GoogleChatCache.onConfigChange();
        initial = false;
    }

    static {
        JFC_GoogleChatConfig.ensureInitialized();
    }
}
