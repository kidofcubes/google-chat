package io.gitlab.jfronny.googlechat.server;

import io.gitlab.jfronny.googlechat.*;
import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.message.v1.ServerMessageDecoratorEvent;
import net.minecraft.text.Text;

import java.util.concurrent.CompletableFuture;

import static io.gitlab.jfronny.googlechat.GoogleChat.hasTarget;
import static io.gitlab.jfronny.libjf.LibJf.LOGGER;

public class GoogleChatServer implements DedicatedServerModInitializer {
    @Override
    public void onInitializeServer() {
        // Default phase is executed between CONTENT and STYLING
        // Perform translation there instead of during CONTENT to better support other mods (such as chat-transform)
        // If this causes an incompatibility, I'll add my own phase
        ServerMessageDecoratorEvent.EVENT.register(Event.DEFAULT_PHASE, (sender, message) -> {
            Text original;
            if (sender != null && hasTarget(GoogleChat.Direction.C2S)) {
                if (hasTarget(GoogleChat.Direction.S2C)) return CompletableFuture.completedFuture(message); // Do not translate back and forth

            }
            if (sender != null) {  // Client messages should first be translated to the server language
                if (hasTarget(GoogleChat.Direction.C2S) && hasTarget(GoogleChat.Direction.S2C)) // Do not translate back and forth
                    return CompletableFuture.completedFuture(message);
                original = message;
                message = GoogleChatCache.c2s(message);
                if (GoogleChatConfig.debugLogs) LOGGER.info("Applied C2S translation from " + original + " to " + message);
            }
            // All messages should be translated to the client language before sending
            original = message;
            message = GoogleChatCache.s2c(message);
            if (GoogleChatConfig.debugLogs) LOGGER.info("Applied S2C translation from " + original + " to " + message);
            return CompletableFuture.completedFuture(message);
        });
    }
}
