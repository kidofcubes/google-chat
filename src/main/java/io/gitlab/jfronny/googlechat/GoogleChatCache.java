package io.gitlab.jfronny.googlechat;

import io.gitlab.jfronny.commons.cache.FixedSizeMap;
import net.minecraft.text.Text;

import java.util.Map;

public class GoogleChatCache {
    private static Map<Text, Text> s2ct = new FixedSizeMap<>(GoogleChatConfig.cacheSize);
    private static Map<Text, Text> c2st = new FixedSizeMap<>(GoogleChatConfig.cacheSize);
    private static Map<String, String> s2cs = new FixedSizeMap<>(GoogleChatConfig.cacheSize);
    private static Map<String, String> c2ss = new FixedSizeMap<>(GoogleChatConfig.cacheSize);

    public static void clear() {
        s2ct.clear();
        c2st.clear();
        s2cs.clear();
        c2ss.clear();
    }

    public static void onConfigChange() {
        GoogleChat.LOGGER.info("Config Change");
        clear();
    }

    public static Text s2c(Text msg) {
        return s2ct.computeIfAbsent(msg, m -> GoogleChat.translateIfNeeded(m, GoogleChat.Direction.S2C, true));
    }

    public static Text c2s(Text msg) {
        return c2st.computeIfAbsent(msg, m -> GoogleChat.translateIfNeeded(m, GoogleChat.Direction.C2S, true));
    }

    public static String s2c(String msg) {
        return s2cs.computeIfAbsent(msg, m -> GoogleChat.translateIfNeeded(m, GoogleChat.Direction.S2C, true));
    }

    public static String c2s(String msg) {
        return c2ss.computeIfAbsent(msg, m -> GoogleChat.translateIfNeeded(m, GoogleChat.Direction.C2S, true));
    }
}
